\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		s1 |
		\set chordChanges = ##f
		s2 g8. g8. fis8 |
		\set chordChanges = ##t
		f1 | g1:7 |
		c1 |
		\set chordChanges = ##f
		g2 g8. g8. fis8 |
		\set chordChanges = ##t
		f1 | g1:7 | g1:7 |

		% somos convocados...
		c1
		\set chordChanges = ##f
		g2 g8. g8. fis8
		\set chordChanges = ##t
		f1 c1

		c1
		\set chordChanges = ##f
		g2 g8. g8. fis8
		\set chordChanges = ##t
		f1 c1

		% somos en este mundo...
		f1 c1 g2 g2:7 c2 c2:7
		f1 c1 g2 g2:7 c1

		% todo un mundo...
		c1
		\set chordChanges = ##f
		g2 g8. g8. fis8
		\set chordChanges = ##t
		f1 c1

		c1
		\set chordChanges = ##f
		g2 g8. g8. fis8
		\set chordChanges = ##t
		f1 c1

		% nunca jamas...
		f1 c1 g2 g2:7 c2 c2:7
		f1 c1 g2 g2:7 c1

		% ven y unete a este canto...
		c1 c1 g1 c2 c2:7
		f1 c1 g2 g2:7 c1

		% ven y unete a este canto...
		c1 c1 g1 c2 c2:7
		f1 c1 g2 g2:7 c1

		% pitos
		r1 r1

		% ven y unete a este canto...
		r1*8

		% ven y unete a este canto...
		c1 c1 g1 c2 c2:7
		f1 c1 g2 g2:7 c1

		% intro
		c1
		\set chordChanges = ##f
		g2 g8. g8. fis8
		\set chordChanges = ##t
		f1 g1:7
		c1
		\set chordChanges = ##f
		g2 g8. g8. fis8
		\set chordChanges = ##t
		f1 g1:7
		c1
	}
