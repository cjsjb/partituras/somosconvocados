\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key c \major

		R1*9  |
%% 10
		r8 g' 16 g' g' 8 g' 16 g' 8 g' g' 16 g' 8 g'  |
		g' 4. g' 8 g' 8. ( g' fis' 8 )  |
		f' 16 f' 8 f' 16 f' 8 f' 16 f' 8 f' f' 16 g' 8 a'  |
		e' 2. r4  |
		r8 e' 16 c' e' 8 c' 16 e' 8 e' c' 16 e' 8 f'  |
%% 15
		d' 4. c' 8 d' ( c' 16 b ~ b 4 )  |
		c' 16 c' 8 c' 16 c' 8 c' 16 c' 8 c' c' 16 d' 8 e'  |
		c' 2. r4  |
		a' 16 a' 8 a' 16 a' 8 a' 16 a' 8 a' 8. a' 8 a'  |
		g' 16 g' 8 g' 8. g' 8 g' g' g' g'  |
%% 20
		g' 16 g' 8 g' 8. g' 8 g' 8. r16 g' 8 g'  |
		g' 2. r4  |
		f' 16 f' 8 f' 16 f' 8 f' 16 f' 8 f' 8. f' 8 f'  |
		e' 16 e' 8 e' 8. e' 8 e' e' e' e'  |
		d' 16 d' 8 d' 8. d' 8 d' 8. r16 c' 8 b  |
%% 25
		c' 2. r4  |
		r8 g' 16 g' g' 8 g' 16 g' 8 g' g' 16 g' 8 g'  |
		g' 4. g' 8 ( g' 8. g' fis' 8 )  |
		f' 16 f' 8 f' 16 f' 8 f' 16 f' 8 f' f' 16 g' 8 a'  |
		e' 2. r4  |
%% 30
		r8 e' 16 c' e' 8 c' 16 e' 8 e' c' 16 e' 8 f'  |
		d' 4. c' 8 ( d' c' 16 b ~ b 4 )  |
		c' 16 c' 8 c' 16 c' 8 c' 16 c' 8 c' c' 16 d' 8 e'  |
		c' 2. r4  |
		a' 16 a' 8 a' 16 a' 8 a' 16 a' 8 a' a' 16 a' 8 a' 16 g' ~  |
%% 35
		g' 16 g' 8 g' 16 g' 8 g' 16 g' 8 g' g' 16 g' 8 g' 16 g' ~  |
		g' 16 g' 8 g' 16 g' 8 g' 16 g' 8 g' g' 16 g' 8 g'  |
		g' 2. r4  |
		f' 16 f' 8 f' 16 f' 8 f' 16 f' 8 f' f' 16 f' 8 f' 16 e' ~  |
		e' 16 e' 8 e' 16 e' 8 e' 16 e' 8 e' e' 16 e' 8 e' 16 d' ~  |
%% 40
		d' 16 d' 8 d' 16 d' 8 d' 16 d' 8 d' d' 16 c' 8 b  |
		c' 2. r4  |
		e' 2 e' 8 e' c' e'  |
		e' 4 e' e' 8 e' c' e'  |
		d' 4 d' d' 8 d' c' d'  |
%% 45
		e' 2. r4  |
		f' 8 f' f' f' f' f' d' f'  |
		e' 8 e' e' e' e' e' c' e'  |
		d' 8 d' d' d' d' d' c' d'  |
		e' 2. r4  |
%% 50
		e' 2 e' 8 e' c' e'  |
		e' 4 e' e' 8 e' c' e'  |
		d' 4 d' d' 8 d' c' d'  |
		e' 2. r4  |
		f' 8 f' f' f' f' f' d' f'  |
%% 55
		e' 8 e' e' e' e' e' c' e'  |
		d' 8 d' d' d' d' d' c' b  |
		c' 2. r4  |
		R1*2  |
%% 60
		e' 2 e' 8 e' c' e'  |
		e' 4 e' e' 8 e' c' e'  |
		d' 4 d' d' 8 d' c' d'  |
		e' 2. r4  |
		f' 8 f' f' f' f' f' d' f'  |
%% 65
		e' 8 e' e' e' e' e' c' e'  |
		d' 8 d' d' d' d' d' c' d'  |
		e' 2. r4  |
		e' 2 e' 8 e' c' e'  |
		e' 4 e' e' 8 e' c' e'  |
%% 70
		d' 4 d' d' 8 d' c' d'  |
		e' 2. r4  |
		f' 8 f' f' f' f' f' d' f'  |
		e' 8 e' e' e' e' e' c' e'  |
		d' 8 d' d' d' d' d' c' b  |
%% 75
		c' 2. r4  |
		R1*10  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
So -- mos con -- vo -- ca -- dos por el Se -- ñor Je -- sús, __
so -- mos a -- gen -- tes de re -- con -- ci -- lia -- ción.
So -- mos con -- vo -- ca -- dos por el Se -- ñor Je -- sús, __
so -- mos a -- gen -- tes de re -- con -- ci -- lia -- ción.

So -- mos en es -- te mun -- do men -- sa -- je -- ros,
los tes -- ti -- gos "que a" -- nun -- cia -- mos la ver -- dad de Je -- sús.
So -- mos en es -- te mun -- do men -- sa -- je -- ros,
los tes -- ti -- gos "que a" -- nun -- cia -- mos la ver -- dad de Je -- sús.

To -- "do un" mun -- do el que hay que trans -- for -- mar, __
só -- lo con nues -- tras fuer -- zas no bas -- ta -- rá.
To -- "do un" mun -- do el que hay que trans -- for -- mar, __
só -- lo con nues -- tras fuer -- zas no bas -- ta -- rá.

Nun -- ca ja -- más ol -- vi -- des que nues -- tra ma -- dre san -- ta Ma -- rí -- a
nos for -- ta -- le -- ce "y a" -- lien -- ta siem -- "pre en" el i -- de -- al.
Nun -- ca ja -- más ol -- vi -- des que nues -- tra ma -- dre san -- ta Ma -- rí -- a
nos for -- ta -- le -- ce "y a" -- lien -- ta siem -- "pre en" el i -- de -- al.

Ven "y ú" -- ne -- "te a es" -- te can -- to,
que tu voz pro -- cla -- me al Se -- ñor Je -- sús.
Ven "y a" -- cep -- ta es -- te de -- sa -- fí -- o,
sien - do hi -- jo de Ma -- ría,
a -- nun -- cian -- do el a -- mor.

Ven "y ú" -- ne -- "te a es" -- te can -- to,
que tu voz pro -- cla -- me al Se -- ñor Je -- sús.
Ven "y a" -- cep -- ta es -- te de -- sa -- fí -- o,
tra -- ba -- jan -- do con tu vi -- da
por la re -- con -- ci -- lia -- ción.

Ven "y ú" -- ne -- "te a es" -- te can -- to,
que tu voz pro -- cla -- me al Se -- ñor Je -- sús.
Ven "y a" -- cep -- ta es -- te de -- sa -- fí -- o,
sien - do hi -- jo de Ma -- ría,
a -- nun -- cian -- do el a -- mor.

Ven "y ú" -- ne -- "te a es" -- te can -- to,
que tu voz pro -- cla -- me al Se -- ñor Je -- sús.
Ven "y a" -- cep -- ta es -- te de -- sa -- fí -- o,
tra -- ba -- jan -- do con tu vi -- da
por la re -- con -- ci -- lia -- ción.
	}
>>
