\context Staff = "zamponna" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Zampoña"
	\set Staff.shortInstrumentName = "Z."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-zamponna" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key c \major

		e'' 16 e'' 8 c'' 16 e'' 8 c'' 16 e'' 8 e'' c'' 16 e'' 8 g''  |
		d'' 4. e'' 8 d'' 8. d'' cis'' 8  |
		c'' 16 c'' 8 a' 16 c'' 8 a' 16 c'' 8 c'' a' 16 c'' 8 e''  |
		d'' 4. e'' 8 d'' 4. r8  |
%% 5
		e'' 16 e'' 8 c'' 16 e'' 8 c'' 16 e'' 8 e'' c'' 16 e'' 8 g''  |
		d'' 4. e'' 8 d'' 8. d'' cis'' 8  |
		c'' 16 c'' 8 a' 16 c'' 8 a' 16 c'' 8 c'' a' 16 c'' 8 e''  |
		g'' 1  |
		R1*67  |
		e'' 16 e'' 8 c'' 16 e'' 8 c'' 16 e'' 8 e'' c'' 16 e'' 8 g''  |
		d'' 4. e'' 8 d'' 8. d'' cis'' 8  |
		c'' 16 c'' 8 a' 16 c'' 8 a' 16 c'' 8 c'' a' 16 c'' 8 e''  |
		d'' 4. e'' 8 d'' 4. r8  |
%% 80
		e'' 16 e'' 8 c'' 16 e'' 8 c'' 16 e'' 8 e'' c'' 16 e'' 8 g''  |
		d'' 4. e'' 8 d'' 8. d'' cis'' 8  |
		c'' 16 c'' 8 a' 16 c'' 8 a' 16 c'' 8 c'' a' 16 c'' 8 e''  |
		g'' 1  |
		c'' 1  |
%% 85
		R1  |
		\bar "|."
	}
>>
