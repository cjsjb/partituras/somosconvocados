\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key c \major

		R1*9  |
%% 10
		r8 c' 16 g c' 8 g 16 c' 8 c' g 16 c' 8 d'  |
		b 4. a 8 b ( a 16 g ~ g 4 )  |
		a 16 a 8 a 16 a 8 a 16 a 8 a a 16 b 8 c'  |
		g 2. r4  |
		r8 c' 16 g c' 8 g 16 c' 8 c' g 16 c' 8 d'  |
%% 15
		b 4. a 8 b ( a 16 g ~ g 4 )  |
		a 16 a 8 a 16 a 8 a 16 a 8 a a 16 b 8 c'  |
		g 2. r4  |
		a 16 a 8 a 16 a 8 a 16 a 8 a 8. a 8 a  |
		g 16 g 8 g 8. g 8 g g g g  |
%% 20
		g 16 g 8 g 8. g 8 g 8. r16 g 8 g  |
		g 2. r4  |
		c' 16 c' 8 c' 16 c' 8 c' 16 c' 8 c' 8. c' 8 c'  |
		c' 16 c' 8 c' 8. c' 8 c' c' c' c'  |
		b 16 b 8 b 8. b 8 b 8. r16 a 8 b  |
%% 25
		c' 2. r4  |
		r8 c' 16 g c' 8 g 16 c' 8 c' g 16 c' 8 d'  |
		b 4. a 8 ( b a 16 g ~ g 4 )  |
		a 16 a 8 a 16 a 8 a 16 a 8 a a 16 b 8 c'  |
		g 2. r4  |
%% 30
		r8 c' 16 g c' 8 g 16 c' 8 c' g 16 c' 8 d'  |
		b 4. a 8 ( b a 16 g ~ g 4 )  |
		a 16 a 8 a 16 a 8 a 16 a 8 a a 16 b 8 c'  |
		g 2. r4  |
		a 16 a 8 a 16 a 8 a 16 a 8 a a 16 a 8 a 16 g ~  |
%% 35
		g 16 g 8 g 16 g 8 g 16 g 8 g g 16 g 8 g 16 g ~  |
		g 16 g 8 g 16 g 8 g 16 g 8 g g 16 g 8 g  |
		g 2. r4  |
		c' 16 c' 8 c' 16 c' 8 c' 16 c' 8 c' c' 16 c' 8 c' 16 c' ~  |
		c' 16 c' 8 c' 16 c' 8 c' 16 c' 8 c' c' 16 c' 8 c' 16 b ~  |
%% 40
		b 16 b 8 b 16 b 8 b 16 b 8 b b 16 a 8 b  |
		c' 2. r4  |
		c' 2 c' 8 c' a c'  |
		c' 4 c' c' 8 c' a c'  |
		b 4 b b 8 b a b  |
%% 45
		c' 2. r4  |
		c' 8 c' c' c' c' c' a c'  |
		c' 8 c' c' c' c' c' a c'  |
		b 8 b b b b b a b  |
		c' 2. r4  |
%% 50
		c' 2 c' 8 c' a c'  |
		c' 4 c' c' 8 c' a c'  |
		b 4 b b 8 b a b  |
		c' 2. r4  |
		c' 8 c' c' c' c' c' a c'  |
%% 55
		c' 8 c' c' c' c' c' a c'  |
		b 8 b b b b b a b  |
		c' 2. r4  |
		R1*2  |
%% 60
		c' 2 c' 8 c' a c'  |
		c' 4 c' c' 8 c' a c'  |
		b 4 b b 8 b a b  |
		c' 2. r4  |
		c' 8 c' c' c' c' c' a c'  |
%% 65
		c' 8 c' c' c' c' c' a c'  |
		b 8 b b b b b a b  |
		c' 2. r4  |
		c' 2 c' 8 c' a c'  |
		c' 4 c' c' 8 c' a c'  |
%% 70
		b 4 b b 8 b a b  |
		c' 2. r4  |
		c' 8 c' c' c' c' c' a c'  |
		c' 8 c' c' c' c' c' a c'  |
		b 8 b b b b b a b  |
%% 75
		c' 2. r4  |
		R1*10  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
So -- mos con -- vo -- ca -- dos por el Se -- ñor Je -- sús, __
so -- mos a -- gen -- tes de re -- con -- ci -- lia -- ción.
So -- mos con -- vo -- ca -- dos por el Se -- ñor Je -- sús, __
so -- mos a -- gen -- tes de re -- con -- ci -- lia -- ción.

So -- mos en es -- te mun -- do men -- sa -- je -- ros,
los tes -- ti -- gos "que a" -- nun -- cia -- mos la ver -- dad de Je -- sús.
So -- mos en es -- te mun -- do men -- sa -- je -- ros,
los tes -- ti -- gos "que a" -- nun -- cia -- mos la ver -- dad de Je -- sús.

To -- "do un" mun -- do el que hay que trans -- for -- mar, __
só -- lo con nues -- tras fuer -- zas no bas -- ta -- rá.
To -- "do un" mun -- do el que hay que trans -- for -- mar, __
só -- lo con nues -- tras fuer -- zas no bas -- ta -- rá.

Nun -- ca ja -- más ol -- vi -- des que nues -- tra ma -- dre san -- ta Ma -- rí -- a
nos for -- ta -- le -- ce "y a" -- lien -- ta siem -- "pre en" el i -- de -- al.
Nun -- ca ja -- más ol -- vi -- des que nues -- tra ma -- dre san -- ta Ma -- rí -- a
nos for -- ta -- le -- ce "y a" -- lien -- ta siem -- "pre en" el i -- de -- al.

Ven "y ú" -- ne -- "te a es" -- te can -- to,
que tu voz pro -- cla -- me al Se -- ñor Je -- sús.
Ven "y a" -- cep -- ta es -- te de -- sa -- fí -- o,
sien - do hi -- jo de Ma -- ría,
a -- nun -- cian -- do el a -- mor.

Ven "y ú" -- ne -- "te a es" -- te can -- to,
que tu voz pro -- cla -- me al Se -- ñor Je -- sús.
Ven "y a" -- cep -- ta es -- te de -- sa -- fí -- o,
tra -- ba -- jan -- do con tu vi -- da
por la re -- con -- ci -- lia -- ción.

Ven "y ú" -- ne -- "te a es" -- te can -- to,
que tu voz pro -- cla -- me al Se -- ñor Je -- sús.
Ven "y a" -- cep -- ta es -- te de -- sa -- fí -- o,
sien - do hi -- jo de Ma -- ría,
a -- nun -- cian -- do el a -- mor.

Ven "y ú" -- ne -- "te a es" -- te can -- to,
que tu voz pro -- cla -- me al Se -- ñor Je -- sús.
Ven "y a" -- cep -- ta es -- te de -- sa -- fí -- o,
tra -- ba -- jan -- do con tu vi -- da
por la re -- con -- ci -- lia -- ción.
	}
>>
